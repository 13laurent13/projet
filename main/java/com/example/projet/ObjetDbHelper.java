package com.example.projet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class ObjetDbHelper extends SQLiteOpenHelper {

    private static final String TAG = ObjetDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "Museum.db";

    public static final String TABLE_NAME = "Objet";

    public static final String _ID = "_id";
    public static final String COLUMN_ID_NAME = "id_name";
    public static final String COLUMN_OBJ_NAME = "name";
    public static final String COLUMN_OBJ_YEAR = "year";
    public static final String COLUMN_CATEGORIES = "categories";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TIME_FRAME = "time_frame";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_TECH_DETAILS = "tech_details";
    public static final String COLUMN_WORKING = "working";
    public static final String COLUMN_PICTURE = "picture";

    public ObjetDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_OBJECT_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ID_NAME + " TEXT NOT NULL, " +
                COLUMN_OBJ_NAME + " TEXT, " +
                COLUMN_OBJ_YEAR + " TEXT, " +
                COLUMN_CATEGORIES + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_BRAND + " TEXT, " +
                COLUMN_TECH_DETAILS + " TEXT, " +
                COLUMN_WORKING + " TEXT, " +
                COLUMN_PICTURE + " TEXT, " +

                // To assure the application have just one object entry per
                // object name and league, it's created a UNIQUE
                " UNIQUE (" + COLUMN_ID_NAME + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_OBJECT_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME); // drops the old database
        onCreate(db);

    }

    /**
     * Fills ContentValues result from a Team object
     */
    private ContentValues fill(Objet objet) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_NAME, objet.getIdName());
        values.put(COLUMN_OBJ_NAME, objet.getName());
        values.put(COLUMN_OBJ_YEAR, objet.getYear());
        String str = TextUtils.join("&",objet.getCategories());
        values.put(COLUMN_CATEGORIES, str);
        values.put(COLUMN_DESCRIPTION, objet.getDescr());
        String str2 = TextUtils.join("&",objet.getTimeFrame());
        values.put(COLUMN_TIME_FRAME, str2);
        values.put(COLUMN_BRAND, objet.getBrand());
        String str3 = TextUtils.join("&",objet.getTechDetails());
        values.put(COLUMN_TECH_DETAILS, str3);
        values.put(COLUMN_WORKING, objet.getWorking());
        String str4 = TextUtils.join("&",objet.getPicture());
        values.put(COLUMN_PICTURE, str4);

        return values;
    }


    public boolean addObjet(Objet objet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(objet);

        Log.d(TAG, "adding: "+objet.getName()+" with id="+objet.getIdName());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (team, championship)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a team inside the data base
     * @return the number of updated rows
     */
    public int updateObjet(Objet objet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(objet);

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(objet.getId()) }, CONFLICT_IGNORE);
    }

    /**
     * Returns a cursor on all the teams of the data base
     */
    public Cursor fetchAllObjet() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_OBJ_NAME +" ASC", null);

        Log.d(TAG, "call fetchAllObjet()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }


    public ArrayList<Object> getAllObjet(String tri) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, tri, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        ArrayList<Object> res = new ArrayList<Object>();
        while(!cursor.isAfterLast()){
            res.add(cursorToObjet(cursor));
            cursor.moveToNext();
        }

        return res;
    }

    public ArrayList<Object> getObjetByCat(ArrayList<String> cat) {

        ArrayList<Object> res = new ArrayList<Object>();
        SQLiteDatabase db = this.getReadableDatabase();
        for(String value : cat){
            res.add(value);
            String[] arg = {"%"+value+"%"};
            Cursor cursor = db.query(TABLE_NAME, null,
                    COLUMN_CATEGORIES+" like ?", arg, null, null, COLUMN_OBJ_NAME +" ASC", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            while(!cursor.isAfterLast()){
                res.add(cursorToObjet(cursor));
                cursor.moveToNext();
            }
        }

        return res;
    }

    public ArrayList<Object> getObjetBySelect(String select) {

        ArrayList<Object> res = new ArrayList<Object>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] arg = {"%"+select+"%"};
        Cursor cursor = db.query(TABLE_NAME, null,
                COLUMN_CATEGORIES+" like ? or "+COLUMN_OBJ_NAME+" like ? "+COLUMN_OBJ_YEAR+" like ? "+COLUMN_DESCRIPTION+" like ? "+COLUMN_TIME_FRAME+" like ? "+COLUMN_BRAND+" like ? "+COLUMN_TECH_DETAILS+" like ? ", arg, null, null, COLUMN_OBJ_NAME +" ASC", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while(!cursor.isAfterLast()){
            res.add(cursorToObjet(cursor));
            cursor.moveToNext();
        }

        return res;

    }

    public Objet cursorToObjet(Cursor cursor) {

        String[] cat1 = (cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES))).split("&");
        ArrayList<String> cat2 = new ArrayList<String>();
        for(String value : cat1)
        {
            cat2.add(value);
        }

        String[] time1 = (cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME))).split("&");
        ArrayList<String> time2 = new ArrayList<String>();
        for(String value : time1)
        {
            time2.add(value);
        }

        String[] tech1 = (cursor.getString(cursor.getColumnIndex(COLUMN_TECH_DETAILS))).split("&");
        ArrayList<String> tech2 = new ArrayList<String>();
        for(String value : tech1)
        {
            tech2.add(value);
        }

        String[] pict1 = (cursor.getString(cursor.getColumnIndex(COLUMN_PICTURE))).split("&");
        ArrayList<String> pict2 = new ArrayList<String>();
        for(String value : pict1)
        {
            pict2.add(value);
        }

        Objet objet = new Objet(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ID_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_OBJ_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_OBJ_YEAR)),
                cat2,
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                time2,
                cursor.getString(cursor.getColumnIndex(COLUMN_BRAND)),
                tech2,
                cursor.getString(cursor.getColumnIndex(COLUMN_WORKING)),
                pict2
        );

        return objet;
    }
}
