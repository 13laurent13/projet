package com.example.projet;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.MyViewHolder>{

    public static ArrayList<String> img;
    public static String img2;
    public static String obj_id;

    public RvAdapter(ArrayList<String> img, String obj_id){
        this.img = img;
        this.obj_id = obj_id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_image, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        img2 = img.get(position);
        holder.display(img2);

    }

    @Override
    public int getItemCount(){
        if(img != null){
            return img.size();
        }
        else{
            return 0;
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        public final TextView desc;
        public final ImageView image;

        public MyViewHolder(final View view){
            super(view);
            desc = view.findViewById(R.id.desc);
            image = view.findViewById(R.id.image);
        }

        public void display(String img2){
            String[] arr = img2.split(":");

            desc.setText(arr[1]);
            try{
                String urlI = WebServiceUrlMuseum.buildGetImage(obj_id, arr[0]).toString();
                Picasso.get().load(urlI).into(image);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
