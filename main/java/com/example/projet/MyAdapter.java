package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final int ITEM_TYPE_OBJET = 1;
    private static final int ITEM_TYPE_STRING = 2;
    public static ArrayList<Object> listObj;
    private ArrayList<Object> savelistObj;
    public static Object obj;


    public MyAdapter(ArrayList<Object> listObj){
        this.listObj = listObj;
        savelistObj = new ArrayList<>(listObj);
    }

    @Override
    public int getItemViewType(int position) {
        if (listObj.get(position) instanceof Objet) {
            return ITEM_TYPE_OBJET;
        } else {
            return ITEM_TYPE_STRING;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if(viewType == ITEM_TYPE_OBJET){
            View view = inflater.inflate(R.layout.objet_on_list, parent, false);
            return new MyObjetViewHolder(view);
        }
        else{
            View view = inflater.inflate(R.layout.objet_on_list, parent, false);
            return new MyTitreViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position){
        obj = listObj.get(position);

        if(obj instanceof Objet){
            ((MyObjetViewHolder) holder).display((Objet) obj);

        }
        else{
            ((MyTitreViewHolder) holder).display((String) obj);
        }

    }



    @Override
    public int getItemCount(){
        if(listObj != null){
            return listObj.size();
        }
        else{
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return filtre;
    }
    private Filter filtre = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Object> filteredList = new ArrayList<Object>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(savelistObj);
            }
            else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Object item : listObj) {
                    if(item instanceof Objet) {
                        if (((Objet) item).getName().toLowerCase().contains(filterPattern) /*|| ((Objet) item).getYear().contains(filterPattern)*/ || TextUtils.join(" - ",((Objet) item).getCategories()).toLowerCase().contains(filterPattern) /*|| ((Objet) item).getDescr().toLowerCase().contains(filterPattern) || TextUtils.join(" - ",((Objet) item).getTimeFrame()).toLowerCase().contains(filterPattern) || ((Objet) item).getBrand().toLowerCase().contains(filterPattern) || TextUtils.join(" - ",((Objet) item).getTechDetails()).toLowerCase().contains(filterPattern)*/) {
                            filteredList.add(item);
                        }
                        else if(((Objet) item).getYear() != null && ((Objet) item).getYear().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Objet) item).getDescr() != null && ((Objet) item).getDescr().toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Objet) item).getTimeFrame() != null && TextUtils.join(" - ",((Objet) item).getTimeFrame()).toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Objet) item).getBrand() != null && ((Objet) item).getBrand().toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Objet) item).getTechDetails() != null && TextUtils.join(" - ",((Objet) item).getTechDetails()).toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listObj.clear();
            listObj.addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }
    };

    public static class MyObjetViewHolder extends RecyclerView.ViewHolder{

        public final TextView name;
        public final TextView categories;
        public final ImageView thumb;

        public MyObjetViewHolder(final View view){
            super(view);
            name = view.findViewById(R.id.name);
            categories = view.findViewById(R.id.categories);
            thumb = view.findViewById(R.id.thumb);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(view.getContext(), Detail.class);
                    intent.putExtra("OBJET", (Objet) listObj.get(getAdapterPosition()));
                    v.getContext().startActivity(intent);
                }
            });
        }

        public void display(Objet objet){
            name.setText(objet.getName());
            categories.setText(TextUtils.join(" - ",objet.getCategories()));
            try{
                String urlT = WebServiceUrlMuseum.buildGetThumbnail(objet.getIdName()).toString();
                Picasso.get().load(urlT).into(thumb);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static class MyTitreViewHolder extends RecyclerView.ViewHolder{

        public final TextView TITLE;

        public MyTitreViewHolder(View view){
            super(view);
            TITLE = view.findViewById(R.id.TITLE);
        }

        public void display(String titre){
            TITLE.setText(titre);
        }
    }
}
