package com.example.projet;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrlMuseum {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Get all id from collection
    // https://demo-lia.univ-avignon.fr/cerimuseum/ids
    private static final String IDS = "ids";

    // Build URL
    public static URL buildGetAllId() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get all categories
    // https://demo-lia.univ-avignon.fr/cerimuseum/categories
    private static final String CAT = "categories";
    public static URL buildGetAllCat() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CAT);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get catalog from museum
    // https://demo-lia.univ-avignon.fr/cerimuseum/catalog
    private static final String CATALOG = "catalog";
    public static URL buildGetAllCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get info on specific item
    private static final String ITEM = "items";
    public static URL buildGetItemInfo(String ItemId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEM)
                .appendPath(ItemId);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get thumbnail of specific item
    private static final String ITEM_T = "items";
    private static final String THUMBNAIL = "thumbnail";
    public static URL buildGetThumbnail(String ItemId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEM_T)
                .appendPath(ItemId)
                .appendPath(THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get image of specific item
    private static final String ITEM_I = "items";
    private static final String IMAGE = "images";
    public static URL buildGetImage(String ItemId, String ImageId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEM_I)
                .appendPath(ItemId)
                .appendPath(IMAGE)
                .appendPath(ImageId);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get next 10 demontrations
    private static final String DEMO = "demos";
    public static URL buildGetDemo() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(DEMO);
        URL url = new URL(builder.build().toString());
        return url;
    }
}
