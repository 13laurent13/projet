package com.example.projet;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.example.projet.Objet;

//Parse info from /items/<itemID>

public class JSONResponseHandlerObjet {

    private static final String TAG = JSONResponseHandlerObjet.class.getSimpleName();

    private Objet objet;


    public JSONResponseHandlerObjet(Objet objet) {
        this.objet = objet;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readObjets(reader);
        } finally {
            reader.close();
        }
    }

    public void readObjets(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            readObjetInfo(reader);
        }
        reader.endObject();
    }


    private void readObjetInfo(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            String field = reader.nextName();

            if (field.equals("brand")) {
                objet.setBrand(reader.nextString());
            } else if (field.equals("description")) {
                objet.setDescr(reader.nextString());
            } else if (field.equals("timeFrame")) {
                reader.beginArray();
                ArrayList<String> TF = new ArrayList<String>();
                while(reader.hasNext()){
                    TF.add(Integer.toString(reader.nextInt()));
                }
                objet.setTimeFrame(TF);
                reader.endArray();
            } else if (field.equals("pictures")) {
                reader.beginObject();
                ArrayList<String> Pict = new ArrayList<String>();
                while(reader.hasNext()){
                    String num = reader.nextName();
                    Pict.add(num.concat(":".concat(reader.nextString())));
                }
                objet.clearPict();
                objet.setPicture(Pict);
                reader.endObject();
            } else if (field.equals("technicalDetails")) {
                reader.beginArray();
                ArrayList<String> TD = new ArrayList<String>();
                while(reader.hasNext()){
                    TD.add(reader.nextString());
                }
                objet.setTechDetails(TD);
                reader.endArray();
            } else if (field.equals("name")) {
                objet.setName(reader.nextString());
            } else if (field.equals("categories")) {
                reader.beginArray();
                ArrayList<String> Cat = new ArrayList<String>();
                while(reader.hasNext()){
                    Cat.add(reader.nextString());
                }
                objet.setCategories(Cat);
                reader.endArray();
            } else if (field.equals("year")) {
                objet.setYear(Integer.toString(reader.nextInt()));
            } else if (field.equals("working")) {
                objet.setWorking(Boolean.toString(reader.nextBoolean()));
            } else {
                reader.skipValue();
            }

        }
    }
}
