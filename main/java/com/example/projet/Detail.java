package com.example.projet;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Detail extends AppCompatActivity {

    String obj_id;
    RvAdapter adapter;

    @Override
        protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);

            Intent intent = getIntent();

        final Objet objet = intent.getParcelableExtra("OBJET");
        obj_id = objet.getIdName();

        GetDateAsync DateA = new GetDateAsync();
        DateA.execute();

        TextView name = findViewById(R.id.name);
        name.setText(objet.getName());

        ImageView img = findViewById(R.id.img);
        try{
            String urlImg = WebServiceUrlMuseum.buildGetThumbnail(obj_id).toString();
            Picasso.get().load(urlImg).fit().into(img);
        }
        catch (Exception e){
            e.printStackTrace();
        }
		if(objet.getYear() != null){
			TextView annee = findViewById(R.id.annee2);
			annee.setText(objet.getYear());
		}

        TextView categories = findViewById(R.id.categories2);
        categories.setText(TextUtils.join(" - ",objet.getCategories()));

        TextView descr = findViewById(R.id.description2);
        descr.setText(objet.getDescr());

        TextView timeFrame = findViewById(R.id.timeFrame2);
        timeFrame.setText(TextUtils.join(" - ",objet.getTimeFrame()));

        if(objet.getBrand() != null){
			TextView brand = findViewById(R.id.brand2);
			brand.setText(objet.getBrand());
		}

        if(objet.getTechDetails() != null){
			TextView techDetail = findViewById(R.id.techDetails2);
			techDetail.setText(TextUtils.join(" - ",objet.getTechDetails()));
		}

        if(objet.getWorking() != null){
			TextView work = findViewById(R.id.working2);
			if(objet.getWorking() != null){
                if(objet.getWorking().toLowerCase().contains("true")){
                    work.setText("OUI");
                }
                else{
                    work.setText("NON");
                }
            }

		}

        /*ListAdapter listA = new ListAdapter(this, objet.getPicture(), obj_id);
        ListView list = findViewById(R.id.list);
        list.setAdapter(listA);*/

        /*RecyclerView rv = (RecyclerView) findViewById(R.id.recviewlist);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RvAdapter(objet.getPicture(), obj_id);
        rv.setAdapter(adapter);*/

		if(!((objet.getPicture().get(0)).equals("null"))){
			Button button = findViewById(R.id.button);
			button.setOnClickListener(new View.OnClickListener(){
				public void onClick(View v){
					Intent intent = new Intent(getApplicationContext(), Image.class);
					intent.putExtra("IMG", objet);
					v.getContext().startActivity(intent);
				}
			});
		}
		else{
            Button button = findViewById(R.id.button);
            button.setText("Aucune photo disponible");
        }

    }

    public class GetDateAsync extends AsyncTask<String, String, String> {

        ArrayList<String> DATE = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url;
            HttpURLConnection urlConnection = null;
            try{
                url = WebServiceUrlMuseum.buildGetDemo();
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream input = urlConnection.getInputStream();
                JSONResponseHandlerDate JSONdate = new JSONResponseHandlerDate(DATE);
                JSONdate.readJsonStream(input);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            return "true";
        }

        @Override
        protected void onPostExecute(String s) {
            for(String elt : DATE){
                String[] elt2 = elt.split(":");
                if((elt2[0]).equals(obj_id)){
                    TextView date = findViewById(R.id.next2);
                    date.setText(elt2[1]);
                }
            }
        }
    }
}
